from django.apps import AppConfig


class IrAppConfig(AppConfig):
    name = 'IR_app'
