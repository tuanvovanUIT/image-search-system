from django.shortcuts import render
from django.http import HttpResponse
from IR_Solve.class_resulst import result
from IR_Solve.main import solve
import os

# Create your views here.
def home(request):
    # l = ["abc", "def"]
    # print(l)


    if(request.method == 'POST'):
        print ("Da post ") 
        SL = int(request.POST['SL'])
        print (request.FILES.getlist("files"))
        id = check_id()
        if(id == -1):
            #Nếu không còn chỗ trống thì xoá hết file case trong media
            clear_file()
            id = 0

        # Tải file lên
        for (count, x) in enumerate(request.FILES.getlist("files")):
            print ('so ' + str(count))
            def process (f):
                with open(r'media/query/i' + str(id) + '_' + str(count), 'wb+') as destination:
                    for chunk in f.chunks():
                        None
                        destination.write(chunk)
            process(x)

        # xử lí
        try:
            rankList = solve("media/query/i"+str(id) + "_0", SL)
        except:
            remmove_file(id)
            # return HttpResponse("Lỗi xử lí, vui lòng thử lại sau")
            return render(request, "IR_app/error.html")
            
            
        # xoá các file input đã xử lí xong
        remmove_file(id)

        return render(request, "IR_app/resulst.html", {'results': rankList})
    return render(request, "IR_app/index.html")


# check id file before upload
def check_id():
    """
        ouput: 0<= id <10
            nếu có nhiều hơn 10 client cùng xử lí thì id = -1
    """
    list_file = os.listdir('media/query/')
    print(list_file)
    i = 0
    while(i<10):
        flag = False
        for j in list_file:
            if(j[0] == 'i' and j[1]== str(i) ):
                flag = True
                break
        if(flag == False):
            print('id tìm được ', i)
            return i
        else:
            i = i + 1

    return -1

def remmove_file(id):
    """
        input: id (int)
        xoá các file có cùng chung id trong media
    """
    list_file = os.listdir('media/')
    print(list_file)
    for i in list_file:
        if(i[0] == 'i' and i[1] == str(id)):
            os.remove(r'media/'+i)

def clear_file():
    """
        Xoá toàn bộ file trong media
    """
    list_file = os.listdir('media/query/')
    print(list_file)
    for i in list_file:
        os.remove(r'media/query/'+i)
