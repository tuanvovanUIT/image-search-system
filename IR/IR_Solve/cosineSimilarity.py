import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from IR_Solve.class_resulst import result

def cosineSimilarity(d1,d2):
    # print(d1)
    # print(d2)
    return np.dot(d1,d2)/(np.linalg.norm(d1)*np.linalg.norm(d2))

def findRankList(id,list):
    temp = []
    for i,j in zip(id,list):
        temp.append(result(i,j))
    ranklist = sorted(temp, key= result_relevant, reverse=True)
    return ranklist

def result_relevant(r):
    return r.relevant