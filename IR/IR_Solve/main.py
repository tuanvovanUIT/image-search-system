import numpy as np
import cv2
from IR_Solve import bagOfword
from IR_Solve import cosineSimilarity
import pickle
from IR_Solve import getQuery
from IR_Solve import readData
from IR_Solve import valuate
import os

def solve(queryPath, k):

  path3 = 'IR_Solve/img_name.txt'

  # Train model gom cum (nếu lần đầu khởi chạy)

  # bagOfword.buildDictionary(path3)

  # Load model đã lưu lên sử dụng
  model = pickle.load(open('IR_Solve/kmeans.sav','rb'))

  # Tìm vector space và idf
  # bow,idf= bagOfword.findVectorSpaceModle(path,model)

  # Lưu idf lại sử dụng cho lần sau
  # f = open('idf.txt','w')
  # for i in idf:
  #   f.write(str(i))
  #   f.write(" ")
  # f.close()

  # # Lưu vectorspace lại cho sử dụng lần sau
  # f = open('vectorspace.txt','w')
  # for i in range(bow.shape[0]):
  #   for j in range(bow.shape[1]):
  #     f.write(str(bow[i,j]))
  #     f.write(" ")
  #   f.write("\n")
  # f.close()

  # Load idf lên sử dụng
  f = open('IR_Solve/idf.txt','r')
  temp = f.readline().split()
  IDF = []
  for i in temp:
    IDF.append(float(i))
  f.close()

  # Load vector space
  voc = []
  f = open('IR_Solve/vectorspace.txt','r')
  temp = f.readlines()
  for i in temp:
    temp2 = []
    for j in i.split():
      temp2.append(float(j))
    voc.append(temp2)
  voc = np.asarray(voc)
  f.close()

  ranklist = valuate.valuatePerform(queryPath, None,path3,IDF,model,voc,k)
  return ranklist

# solve(r'C:\Users\PHUOC BAN\Desktop\error.png', 10)