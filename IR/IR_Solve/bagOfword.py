import cv2
import numpy as np
from sklearn.cluster import MiniBatchKMeans
from IR_Solve import readData
import math
import pickle
def buildDictionary(path):

    # Đọc toàn bộ ảnh
    # Trích xuất đặc trưng thu được các keypionts 
    # Tiến hành gom cụm để thu được tập từ vựng

    bowTrainer = []
    imgPath = readData.readData(path)
    for i in range(len(imgPath)):
        img = readData.cv2.imread(imgPath[i],0)
        sift = cv2.xfeatures2d.SIFT_create()
        kps,fts = sift.detectAndCompute(img,None)
        bowTrainer.append(fts)
    bowTrainer = np.asarray(bowTrainer)
    bowTrainer = np.concatenate(bowTrainer)
    kmeans = MiniBatchKMeans(n_clusters=1000,init_size = 3000,batch_size = 1000, random_state=0).fit(bowTrainer)
    pickle.dump(kmeans,open('kmeans.sav','wb'))
    # return kmeans

def findVectorSpaceModle(path,model):

    # Duyệt qua toàn bộ bức ảnh để thu được keypoints
    # Tiến hành gom cụm các keypoints dựa vào tập vocabulary trước đó
    # Tính toán histogram của bức ảnh (Thống kê tần suất xuất hiện của các keypoints trong một ảnh)
    # Tính TF
    # Tính IDF
    # Tính TF-IDF

    feature_vector = []
    imgPath = readData.readData(path)
    for i in range(len(imgPath)):
        img = cv2.imread(imgPath[i],0)
        sift = cv2.xfeatures2d.SIFT_create()
        kps,fts = sift.detectAndCompute(img,None)
        predict_kmeans = model.predict(fts)
        hist, bin_edges=np.histogram(predict_kmeans, bins=1000)
        feature_vector.append(hist)
    feature_vector = np.array(feature_vector, dtype = np.float64)

    # Tính TF 
    for i in range(feature_vector.shape[0]):
        for j in range(feature_vector.shape[1]):
            if feature_vector[i][j]!= 0:
                feature_vector[i][j] = 1.0 + float(math.log(feature_vector[i][j]))
    
    idf = []
    # # Tính IDF
    for i in range(feature_vector.shape[1]):
        dem = 0
        for j in range(feature_vector.shape[0]):
            if (feature_vector[j][i]!= 0):
                dem = dem + 1
        idf.append(1.0 + float(math.log(feature_vector.shape[0]/dem)))
    # tính tf - idf 
    for i in range(feature_vector.shape[1]):
        for j in range(feature_vector.shape[0]):
            feature_vector[j][i] = feature_vector[j][i]*idf[i]
    return feature_vector,idf