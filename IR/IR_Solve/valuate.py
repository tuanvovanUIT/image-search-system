from IR_Solve import cosineSimilarity
import os
from IR_Solve import getQuery
def valuatePerform(path1,path2,path3,idf,model,vector_space, k):
  # Xử lý đường dẫn
  # Mở file chứa câu query
  # Thực hiện truy vấn bằng ảnh sau đó trả về ranklist

  # Lay ten file anh trong dataset
  list_name = []
  f = open(path3,'r')
  temp = f.read()
  for i in temp.split():
    list_name.append(i)
  f.close()

  cosine_list = []
  # Lấy query
  query = getQuery.getQuery(path1, idf,model)
    # Try vấn, trả về ranklist
  for kp in vector_space:
    cosine_list.append(cosineSimilarity.cosineSimilarity(kp,query))
  ranklist = cosineSimilarity.findRankList(list_name,cosine_list)

  # for i in ranklist[:10]:
  #   i.xuat()

  if k > len(ranklist):
    return ranklist
  return ranklist[:k]


  # # Lấy top 10 ranklist
  # k = 0
  # top_k = []
  # for i in ranklist:
  #   if k <10:
  #     top_k.append((i,ranklist[i]))
  #     k = k + 1
  # return top_k