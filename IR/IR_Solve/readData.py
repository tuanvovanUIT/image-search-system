import cv2
import numpy as np
import os

def readData(path):
    imgPath = os.listdir(path)
    for i in range(0, len(imgPath)):
        imgPath[i] = path + '/' + imgPath[i]
    return imgPath

