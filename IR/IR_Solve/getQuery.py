import numpy as np
import cv2
import math
from IR_Solve import readData

def getQuery(path,idf,model):
    # Chuyển về bảng tần suất

    feature_vector = []
    img = cv2.imread(path,0)
    sift = cv2.xfeatures2d.SIFT_create()
    kps,fts = sift.detectAndCompute(img,None)
    predict_kmeans = model.predict(fts)
    hist, bin_edges=np.histogram(predict_kmeans, bins=1000)
    feature_vector.append(hist)
    feature_vector = np.array(feature_vector, dtype = np.float64)
    
    # Tính TF 
    for i in range(feature_vector.shape[0]):
        for j in range(feature_vector.shape[1]):
            if feature_vector[i][j]!= 0:
                feature_vector[i][j] = 1.0 + float(math.log(feature_vector[i][j]))
    
    
    # tính tf - idf 
    for i in range(feature_vector.shape[1]):
        for j in range(feature_vector.shape[0]):
            feature_vector[j][i] = feature_vector[j][i]*idf[i]
    return np.concatenate(feature_vector)
